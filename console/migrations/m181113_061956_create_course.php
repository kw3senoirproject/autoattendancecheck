<?php

use yii\db\Migration;

/**
 * Class m181113_061956_create_course
 */
class m181113_061956_create_course extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('course',[
            'id' => $this->primaryKey(),
            'coursecode' => $this->string(6),
            'coursename' => $this->string(),
            'year' => $this->string(4),
            'semester' => $this->smallInteger(3)
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('course');
        $this->dropForeignKey('fk-course-section_instruction');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181113_061956_create_course cannot be reverted.\n";

        return false;
    }
    */
}
