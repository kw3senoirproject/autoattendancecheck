<?php

use yii\db\Migration;

/**
 * Class m181113_064113_create_section
 */
class m181113_064113_create_section extends Migration
{
    /**
     * {@inheritdoc}
     */
    /*public function safeUp()
    {

    }*/

    /**
     * {@inheritdoc}
     */
    /*public function safeDown()
    {
        echo "m181113_064113_create_section cannot be reverted.\n";

        return false;
    }*/

    // Use up()/down() to run migration code without a transaction.

    public function up()
    {
        $this->createTable('section',[
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'section_no' => $this->tinyInteger(3),
            'room' => $this->smallInteger(4),
            'time_begin' => $this->time(),
            'time_end' => $this->time(),
            'day' => $this->string(3),
            'campus' => $this->string(3),

        ]);

        $this->addForeignKey('fk-section-course_id',
            'section', 'course_id',
            'course', 'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-section-course_id','course_id');
        $this->dropTable('section');
    }
}
