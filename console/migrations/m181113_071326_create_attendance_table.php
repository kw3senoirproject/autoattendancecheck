<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance`.
 */
class m181113_071326_create_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('attendance', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'section_id' => $this->integer(),
            'timestamp' => $this->timestamp(),
            'status' => $this->boolean()
        ]);

        $this->addForeignKey('fk-attendance-user_id',
            'attendance','user_id',
            'user','id'
        );

        $this->addForeignKey('fk-attendance-section_id',
            'attendance','section_id',
            'section', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk-attendance-user_id');
        $this->dropForeignKey('fk-attendance-section_id');
        $this->dropTable('attendance');
    }
}
