<?php

use yii\db\Migration;

/**
 * Class m181114_110540_create_table_photo_update
 */
class m181114_110540_create_table_photo_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('photo_update',[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'timestamp' => $this->timestamp(),
            'photo_url' => $this->text(),
            'status' => $this->boolean()
        ]);

        $this->addForeignKey('fk-phoyo_update-user_id',
            'photo_update','user_id',
            'user','id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('photo_update');
        $this->dropForeignKey('fk-phoyo_update-user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181114_110540_create_table_photo_update cannot be reverted.\n";

        return false;
    }
    */
}
