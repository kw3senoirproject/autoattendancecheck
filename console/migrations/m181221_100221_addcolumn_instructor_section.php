<?php

use yii\db\Migration;


class m181221_100221_addcolumn_instructor_section extends Migration
{
    
    public function up()
    {
        $this->addColumn('section', 'instructor', $this->integer());
        $this->addForeignKey('fk-section-instructor',
            'section', 'instructor',
            'user', 'id'
        );
    }

    
    public function down()
    {
        $this->dropForeignKey('fk-section-instructor','instructor');
        $this->dropColumn('section', 'instructor');
    }

    
}
