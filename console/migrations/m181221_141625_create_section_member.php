<?php

use yii\db\Migration;

/**
 * Class m181221_141625_create_section_member
 */
class m181221_141625_create_section_member extends Migration
{
    
    public function up()
    {
        $this->createTable('section_member',[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'section_id' =>$this->integer()

        ]);

        $this->addForeignKey('fk-section_member-user_id',
            'section_member', 'user_id',
            'user', 'id'
        );

        $this->addForeignKey('fk-section_member-section_id',
            'section_member', 'section_id',
            'section', 'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-section_member-user_id','user_id');
        $this->dropForeignKey('fk-section_member-section_id','section_id');
        $this->dropTable('section_member');
    }
    
}
