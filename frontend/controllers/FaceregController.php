<?php

namespace frontend\controllers;

use yii\rest\ActiveController;
// use frontend\models\User;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use common\models\User;

class FaceregController extends ActiveController
{
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'only' => ['index', 'view'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

	public $modelClass = 'common\models\User';

	public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actionPost() //create
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new User();
        $user->scenario = USER::USER_CREATE;
        $user->attributes = \yii::$app->request->post();
        if ($user->validate()) {
            $user->save();
            return array('status' => true, 'data' => 'Record is successfully updated');
        } else {
            return array('status' => false, 'data' => $user->getErrors());
        }
    }

    public function actionGet()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = User::find()->all();
        // if (count($user) > 0) {
            return array('status' => true, 'data' => $user);
        // } else {
            // return array('status' => false, 'data' => 'No User Found');
        // }
    }

    public function actionRecognition($id)
    {
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = User::find()->where(['id' => $id])->one();
        // if (count($user) > 0) {
        //     return ['status' => true, 'data' => $user];
        // } else {
        //     return array('status' => false, 'data' => 'No User Found');
        // }

        return $this->render('facerecognition',[
                    'model' => $model,
                ]);
    }

    public function actionPut()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $attributes = \yii::$app->request->post();
        $user = User::find()->where(['ID' => $attributes['id']])->one();
        if (count($user) > 0) {
            $user->attributes = \yii::$app->request->post();
            $user->save();
            return array('status' => true, 'data' => 'User record is updated successfully');
        } else {
            return array('status' => false, 'data' => 'No User Found');
        }
    }
    public function actionDeleteRoll()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $attributes = \yii::$app->request->post();
        $user = User::find()->where(['ID' => $attributes['id']])->one();
        if (count($user) > 0) {
            $user->delete();
            return array('status' => true, 'data' => 'User record is successfully deleted');
        } else {
            return array('status' => false, 'data' => 'No User Found');
        }
    }


    public function actionFacerecognition()
    {
        return $this->render('facerecognition');
    }

    public function actionView($id)
	{
	    return User::findOne($id);
	}

}
