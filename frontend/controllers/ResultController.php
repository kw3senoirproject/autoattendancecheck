<?php

namespace frontend\controllers;

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use frontend\models\Attendance;
use common\models\User;
use frontend\models\Section;
use frontend\models\Course;
use frontend\models\SectionMember;
use Yii;

class ResultController extends \yii\web\Controller
{
    public function actionStudent($coursecode='ITS100')
    {

    	$dataProvider = new ActiveDataProvider([
		    'query' => Attendance::find(),
		    'pagination' => [
		        'pageSize' => 20,
		    ],
		]);
        return $this->render('student',[
        	'dataProvider' => $dataProvider,
            'coursecode' => $coursecode
        ]);


    }

    public function actionTeacher($coursecode,$section_no)
    {       
        $course = Course::find()->where(['coursecode' => $coursecode])->one();

        $section = Section::find()->where(['course_id' => $course->id])
              ->andWhere(['section_no' => $section_no])->one();

        // $query = SectionMember::find()
        //     ->select(['section_member.*','attendance.timestamp'])
        //     ->leftJoin('attendance', 'section_member.user_id = attendance.user_id AND section_member.section_id = attendance.section_id')
        //     ->where(['section_member.section_id' => $section->id]);

        $query = SectionMember::find()
            ->select(['section_member.*','attendance.timestamp','user.student_id','user.firstname','user.lastname'])
            ->leftJoin('attendance', 'section_member.user_id = attendance.user_id AND section_member.section_id = attendance.section_id')
            ->leftJoin('user', 'section_member.user_id = user.id')
            ->where(['section_member.section_id' => $section->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['student_id']]
            /*'pagination' => [
                'pageSize' => 20,
            ],*/
        ]);
        return $this->render('teacher',[
            'dataProvider' => $dataProvider,
            'coursecode' => $coursecode,
            'section_no' => $section_no,
        ]);
    }

}
