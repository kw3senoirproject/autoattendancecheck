<?php

namespace frontend\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string $coursecode
 * @property string $coursename
 * @property string $year
 * @property int $semester
 *
 * @property Section[] $sections
 */
class Course extends \yii\db\ActiveRecord
{
    public $section_no;
    /**
     * {@inheritdoc}
     */


    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester'], 'integer'],
            [['coursecode'], 'string', 'max' => 6],
            [['coursename'], 'string', 'max' => 255],
            [['year'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coursecode' => 'Coursecode',
            'coursename' => 'Coursename',
            'year' => 'Year',
            'semester' => 'Semester',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['course_id' => 'id']);
    }

    public function getCourseIdByCoursecode($coursecode)
    {
        $course = Course::find()->where(['coursecode' => $coursecode])->one();
        return $course->id;
    }

    public function getLink($course)
    {
        $user = Yii::$app->user->identity;
        if ($user->user_type == 'S') {
            $view = 'result/student';
        }else{
            $view = 'result/teacher';
        }
        $coursecode = $course->coursecode ;
        $coursename = $course->coursename ;
        $section = $course->section_no ;
        return Html::a($coursecode ,[$view,
            'coursecode' => $coursecode,
            'sectio_no' => $section,
        ]);
    }
}
