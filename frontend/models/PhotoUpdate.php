<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "photo_update".
 *
 * @property int $id
 * @property int $user_id
 * @property string $timestamp
 * @property string $photo_url
 * @property int $status
 *
 * @property User $user
 */
class PhotoUpdate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo_update';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['timestamp'], 'safe'],
            [['photo_url'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [  
            'id' => 'ID',
            'user_id' => 'User ID',
            'timestamp' => 'Timestamp',
            'photo_url' => 'Photo Url',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFullname($user_id)
    {
        $user = User::find()->where(['id' => $user_id])->one();
        return $user->firstname.' '.$user->lastname;
    }

}
