<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $section_no
 * @property int $room
 * @property string $datetime
 * @property string $day
 * @property string $campus
 *
 * @property Attendance[] $attendances
 * @property Course $course
 * @property User $user
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'section_no', 'room'], 'integer'],
            [['datetime'], 'safe'],
            [['day', 'campus'], 'string', 'max' => 3],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'course_id' => 'Course ID',
            'section_no' => 'Section No',
            'room' => 'Room',
            'datetime' => 'Datetime',
            'day' => 'Day',
            'campus' => 'Campus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
