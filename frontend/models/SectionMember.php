<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "section_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int $section_id
 * @property timestamp $timestamp
 */
class SectionMember extends \yii\db\ActiveRecord
{
    public $timestamp;
    public $firstname;
    public $lastname;
    public $student_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'section_id'], 'integer'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'section_id' => 'Section ID',
        ];
    }

    public function getAttendanceTeacher($attendance)
    {

        if (is_null($attendance)) {
              $attendance = False; 
         } else {
              $attendance = True;  
        }

        return $attendance;
    }

    public function getAttendance()
    {
        return $this->hasOne(Attendance::className(), ['section_id' => 'section_id']);
    }

    public function getUser()
    {
        return $this->hasMany(User::className(), ['user_id' => 'id']);
    }
}
