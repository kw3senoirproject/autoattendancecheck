<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $firstname;
    public $lastname;
    public $student_id;
    public $citizen_id;
    public $passport_id;
    public $title;
    public $user_type;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['student_id', 'trim'],
            ['student_id', 'required'],
            ['student_id', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This student id is already exist.'],

            ['title', 'safe'],

            ['firstname', 'trim'],
            ['firstname', 'required'],

            ['lastname', 'trim'],
            ['lastname', 'required'],

            ['citizen_id', 'trim'],
            ['citizen_id', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This citizen id is already exist.'],

            ['passport_id', 'trim'],
            ['passport_id', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This passport id is already exist.'],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['user_type', 'safe'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->firstname = $this->firstname;
        $user->lastname = $this->lastname;
        $user->student_id = $this->student_id;
        $user->citizen_id = $this->citizen_id;
        $user->passport_id = $this->passport_id;
        $user->title = $this->title;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->user_type = $this->user_type;
        
        return $user->save() ? $user : null ;
    }
}
