<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $student_id
 * @property string $title
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $citizen_id
 * @property string $passport_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $user_type
 */
class User extends \yii\db\ActiveRecord
{
    const USER_CREATE = 'create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'title', 'firstname', 'lastname', 'username', 'auth_key', 'password_hash', 'created_at', 'updated_at', 'user_type'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['student_id'], 'string', 'max' => 10],
            [['title', 'firstname', 'lastname', 'email', 'passport_id', 'username', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['citizen_id'], 'string', 'max' => 13],
            [['auth_key'], 'string', 'max' => 32],
            [['user_type'], 'string', 'max' => 1],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    public function users()
    {
        $users = parent::users();
        $users['create'] = [
            'id',
            'student_id',
            'title',
            'firstname',
            'lastname',
            'email',
            'citizen_id',
            'passport_id',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'status',
            'created_at',
            'updated_at',
            'user_type',
        ];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student ID',
            'title' => 'Title',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'citizen_id' => 'Citizen ID',
            'passport_id' => 'Passport ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_type' => 'User Type',
        ];
    }
}
