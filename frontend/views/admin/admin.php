<?php use yii\helpers\Html;
 ?>


 <?php $user=Yii::$app->user->identity; ?>
<h1> <?php echo 'Name: '.$user->firstname.' '.$user->lastname  ?> </h1>
<br>
<br>
<br>
<div align="center">
	<?= Html::a('Approval', ['/admin/approval'], ['class'=>'btn btn-primary btn-lg']) ?>

	<?= Html::a('Photo Update', ['/admin/photo-update'], ['class'=>'btn btn-info btn-lg ']) ?>
</div>

