<?php 
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;


$this->title = 'Photo Update Approval';
$user = Yii::$app->user->identity;
 ?>


 <div class="admin-approval">
 	
 	<?php if(!Yii::$app->user->isGuest): ?>
 		<div class="container">
            <h1> <?php echo "Photo Update Requests" ?> </h1>
            
            <?php 
            	echo GridView::widget([
				    'dataProvider' => $dataProvider,
				    'columns' => [
				        ['class' => 'yii\grid\SerialColumn'],
				        // Simple columns defined by the data contained in $dataProvider.
				        // Data from the model's column will be used.
				        // 'id',
				        [
				         'label' => 'Name',
				         'value' => function ($model) {
				             return $model->getFullname($model->user_id);
				         }
				       ],
				        'timestamp',
				     	[
						  'class' => 'yii\grid\ActionColumn',
						  'header' => 'Actions',
						  'headerOptions' => ['style' => 'color:#337ab7'],
						  'template' => '<div class="text-center" role="group">{view}{delete}',
						  'buttons' => [
						    'view' => function ($url, $model) {
						        return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>' , ['admin/check-photo']);
						    },

						    'delete' => function ($url, $model) {
						         return Html::a( '<span class="glyphicon glyphicon-trash"></span>' , Yii::$app->request->referrer);
						    }

						  ],
						  
						  ],
				        
				    ],
				]);
             ?>
        </div>
 	<?php endif ?>

 </div>