<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PhotoUpdate */

$this->title = 'Create Photo Update';
$this->params['breadcrumbs'][] = ['label' => 'Photo Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-update-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
