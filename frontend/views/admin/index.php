<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Photo Updates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-update-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Photo Update', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                        
            ['class' => 'yii\grid\SerialColumn'],

            
            [
            'label' => 'Name',
            'value' => function ($model) {
            return $model->getFullname($model->user_id);
            }
            ],
            'timestamp',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
