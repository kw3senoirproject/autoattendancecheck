<?php 
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = 'Individual Attendance Result';
$user = Yii::$app->user->identity;
 ?>


 <div class="result-student">
 	
 	<?php if(!Yii::$app->user->isGuest): ?>
 		<div class="container">

            <h1> <?php echo 'Course: ' .$coursecode ?> </h1>
            <h1> <?php // echo 'Name: ' .$user->firstname.' '.$user->lastname  ?> </h1>
            <?php User::getFullname($user->id) ?>
            <h1> <?php echo "Student ID: " .$user->student_id ?> </h1>
            


            <?php 
            	echo GridView::widget([
				    'dataProvider' => $dataProvider,
				    'columns' => [
				        ['class' => 'yii\grid\SerialColumn'],
				        // Simple columns defined by the data contained in $dataProvider.
				        // Data from the model's column will be used.
				        // 'id',

			             ['label' => 'Date',
			               'attribute' => 'timestamp',
			               'format' => 'date',
			             ],
			             ['label' => 'Time',
			               'attribute' => 'timestamp',
			               'format' => 'time',
			             ],
			             [
					       'label' => 'Attendance',
					       'value' => function ($model) {
					           return $model->getAttendanceValue($model);
					       }
					     ],
				    ],
				]);
             ?>
        </div>
 	<?php endif ?>

 </div>