<?php 
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = 'Individual Attendance Result';
$user = Yii::$app->user->identity;
 ?>


 <div class="result-student">
 	
 	<?php if(!Yii::$app->user->isGuest): ?>
 		<div class="container">
            <h1> <?php echo "Course: ".$coursecode ?> </h1>
            <h3> <?php echo "Section: ".$section_no ?> </h3>
            <h1> <?php echo 'Name: '.User::getFullname($user->id)  ?> </h1>

            <?php 
            	echo GridView::widget([
				    'dataProvider' => $dataProvider,
				    'columns' => [
				        ['class' => 'yii\grid\SerialColumn'],
				        'student_id',
				        [
						   'label' => 'Name',
						   'value' => function ($model) {
						       return User::getFullname($model->user_id);
						   }
						],
						[
							'label' => 'Date',
							'attribute' => 'timestamp',
							'format' => 'date',
							'headerOptions' => ['style' => 'text-align: center'],
						    'contentOptions' => ['style' => 'text-align: center'],
						],
						[
							'label' => 'Time',
							'attribute' => 'timestamp',
							'format' => ['time', 'php:H:i:s'],
							'headerOptions' => ['style' => 'text-align: center'],
						    'contentOptions' => ['style' => 'text-align: center'],
						],
						//'timestamp',
				        [

						    'attribute' => 'Attendance',
						    'headerOptions' => ['style' => 'text-align: center'],
						    'contentOptions' => ['style' => 'text-align: center'],
						    'format' => 'raw',
						    'value' => function ($model, $index, $widget) {
						        return Html::checkbox('attendance[]',
							        	$model->getAttendanceTeacher($model->timestamp),
							        	['value' => $index]
						        	);
						    },

						],
				    ],
				]);
             ?>
        </div>
 	<?php endif ?>

 </div>