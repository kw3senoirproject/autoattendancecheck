<?php
use yii\helpers\Html;
use common\models\LoginForm;
use yii\grid\GridView;


/* @var $this yii\web\View */

$this->title = 'Auto Attendance Check';
?>
<div class="site-index"> 

    <?php if(Yii::$app->user->isGuest): ?>
<!--         <div>
            <div class="inline">
                <img src="../web/images/Siit_tu.gif" class="img-rounded" width="150" height="150" style="display: inline;">
                <img src="../web/images/siit25.png" class="img-rounded" width="268" height="150" style="display: inline;">
            </div>
        </div> -->
        <div class="jumbotron">
            <h2>Welcome to SIIT Smart Classroom System!</h2>
            <h3>Please Login to continue</h3>
        </div>
        <div class="col col-md-6 col-md-offset-3">
            <?php $model = new LoginForm() ?>
            <?php echo  $this->render('/forms/login_form',[
                'model' => $model
            ]) ?>
        </div>
    <?php else: ?>

        <?php
            // check role of user and display page according to role
            $user = Yii::$app->user->identity;
            if ($user->user_type != 'A'): ?>
                <h1> <?php echo 'Name: '.$user->firstname.' '.$user->lastname  ?> </h1>
                <?php if ($user->user_type == 'S'): ?>
                    <h1> <?php echo "Student ID:".$user->student_id ?> </h1>
                <?php endif; ?>
                <br>
                <br>

                <?php 
                echo GridView::widget([ 
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Course Code',
                            'template' => '{coursecode}',  // the default buttons + your custom button
                            'buttons' => [
                                'coursecode' => function($url, $model, $key) {     // render your custom button
                                      return $model->getLink($model);
                                   // return Html::a('coursecode', ['site/login']) ;
                                }
                            ]
                        ],  

                        'coursename',
                        'section_no',

                        

                        
                         
                    ],
                        
             ]);
             ?>

             <?= Html::a('Update Photo >>', ['/admin/photo-update'], ['class'=>'btn btn-primary']) ?>

                <?php //Yii::$app->response->redirect(['result/student']) ?>

            
        <?php elseif ($user->user_type == 'T'):
                echo 'TEACHER & ADMIN'; ?>
                <?php //Yii::$app->response->redirect(['result/teacher']) ?>

        <?php elseif ($user->user_type == 'A'): ?>
                <?php Yii::$app->response->redirect(['admin/admin']) ?>

        <?php endif  ?>

        
    <?php endif ?>
</div>
