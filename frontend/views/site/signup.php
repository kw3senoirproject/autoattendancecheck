<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Username (Student ID for student)') ?>

                <?= $form->field($model, 'password')->passwordInput()->label('Password (Citizen ID or Passport ID for student)') ?>

                <?= $form->field($model, 'student_id')->label('Student ID') ?>

                <?php 

                    $title = [
                        'Mr' => 'Mr', 
                        'Ms' => 'Ms',
                        'Dr' => 'Dr'
                    ]

                 ?>

                <?= $form->field($model, 'title')->dropdownList($title)->label('Title') ?>

                <?= $form->field($model, 'firstname')->label('Firstname') ?>

                <?= $form->field($model, 'lastname')->label('Lastname') ?>

                <?= $form->field($model, 'email')->label('E-mail') ?>

                <?= $form->field($model, 'citizen_id')->label('Citizen ID (for Thai student)') ?>

                <?= $form->field($model, 'passport_id')->label('Passport ID (For foreign student)') ?>

                <?= $form->field($model, 'user_type')->dropdownList([
                        'S' => 'Student', 
                        'T' => 'Teacher',
                        'A' => 'Admin'
                ])->label('Select role of user') ?>
                

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
